package models;

public class Product {
    private int id;
    private String god;
    private String model;
    private String description;
    private int count;
    private int price;

    public Product(String god, String model, String description, int count, int price) {
        this.god = god;
        this.model = model;
        this.description = description;
        this.count = count;
        this.price = price;
    }

    public Product(int id, String god, String model, String description, int count, int price) {
        this(god, model, description, count, price);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getGod() {
        return god;
    }

    public String getModel() {
        return model;
    }


    public String getDescription() {
        return description;
    }

    public int getCount() {
        return count;
    }

    public int getPrice() {
        return price;
    }

    public String getShortDesc(){
        return getDescription().split("\n")[0];
    }
}
