package models;

public class User {
    private int id;
    private boolean isAdmin;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;

    public User(int id, String login, String password, String firstName, String lastName, String email) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        isAdmin = false;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public static User getGuest() {
        return new User(-1, "Guest", null, "Гость", "", null);
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String toString(){
        return firstName + lastName + login + password;
    }

    public String getFullName(){
        return getFirstName() + " " + getLastName();
    }

    public boolean isAdmin(){
        return isAdmin;
    }

    public void setAdmin(boolean adm){
        isAdmin = adm;
    }
}
