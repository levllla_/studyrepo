package repository;

import controllers.configs.ConnectionSingleton;
import models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductRepository {

    public static ArrayList<Product> getProducts() {
        ArrayList<Product> productList = new ArrayList<>();
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            conn = ConnectionSingleton.getConnection();//
            stmt = conn.prepareStatement("select * from products");
            rs = stmt.executeQuery();

            while (rs.next()) {
                Product product = new Product(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getInt(6), rs.getInt(5));
                productList.add(product);
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }

    public static Product getProduct(int id) {
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;
        Product product = null;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement("select * from products where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            while (rs.next()) {
                product = new Product(id, rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getInt(6), rs.getInt(5));
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public static boolean createProduct(Product product) {
        Connection conn;
        PreparedStatement stmt;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement(
                    "insert into products(manufacturer, model, description, available, price) " +
                            "values(?, ?, ?, ?, ?)");
            stmt.setString(1, product.getGod());
            stmt.setString(2, product.getModel());
            stmt.setString(3, product.getDescription());
            stmt.setInt(4, product.getCount());
            stmt.setInt(5, product.getPrice());

            if (stmt.executeUpdate() > 0) {
                stmt.close();
                return true;
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int getLastId() {
        Integer id = null;
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement("select max(id) from products");
            rs = stmt.executeQuery();
            rs.next();
            id = rs.getInt(1);
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public static void deleteProduct(int id) {
        Connection conn;
        PreparedStatement stmt;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement("delete from products where id = ?");
            stmt.setInt(1, id);
            stmt.executeQuery();
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void editProduct(Product product) {
        Connection conn;
        PreparedStatement stmt;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement(
                            "update products  " +
                            "set manufacturer = ?," +
                            " model = ?," +
                            " description = ?," +
                            " available = ?," +
                            " price = ? " +
                            "where id = ?");

            stmt.setString(1, product.getGod());
            stmt.setString(2, product.getModel());
            stmt.setString(3, product.getDescription());
            stmt.setInt(4, product.getCount());
            stmt.setInt(5, product.getPrice());
            stmt.setInt(6, product.getId());

            if (stmt.executeUpdate() > 0) {
                System.out.println("Update true");
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
