package repository;

import controllers.configs.ConnectionSingleton;
import models.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersRepository {
    public static boolean createUser(User user) {
        Connection conn;
        PreparedStatement stmt;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement(
                    "insert into users(username, password, email, first_name, last_name) " +
                    "values(?, ?, ?, ?, ?)");

            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getFirstName());
            stmt.setString(5, user.getLastName());

            if (stmt.executeUpdate() > 0) {
                stmt.close();
                return true;
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String authUser(User user) {
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement("select password from users where username = ?");
            stmt.setString(1, user.getLogin());
            rs = stmt.executeQuery();

            if(!rs.isBeforeFirst())
                return "invalidLogin";

            while (rs.next())
                if (rs.getString(1).equals(user.getPassword()))
                    return "success";
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return "invalidPassword";
    }

    public static User getUser(String login) {
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;
        User user = null;
        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement(
                    "select id, password, first_name, last_name, email, admin " +
                    "from users " +
                    "where username = ?");
            stmt.setString(1, login);
            rs = stmt.executeQuery();

            while (rs.next()){
                user = new User(rs.getInt(1), login, rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                if(rs.getInt(6) != 0)
                    user.setAdmin(true);
            }

            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}
