package repository;

import controllers.configs.ConnectionSingleton;
import models.Comment;
import models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CommentRepository {
    public static ArrayList<Comment> getProductComments(int productId){
        ArrayList<Comment> comments = new ArrayList<>();
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement(
                    "select id, content, user_name " +
                    "from comments " +
                    "where product_id = ?");

            stmt.setInt(1, productId);
            rs = stmt.executeQuery();

            while (rs.next())
                comments.add(new Comment(rs.getInt(1), rs.getString(3), rs.getString(2)));

            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return comments;
    }

    public static boolean createComment(String userName, String content, int product_id) {
        Connection conn;
        PreparedStatement stmt;

        try {
            conn = ConnectionSingleton.getConnection();
            stmt = conn.prepareStatement("insert into comments(user_name, content, product_id) values(?, ?, ?)");
            stmt.setString(1, userName);
            stmt.setString(2, content);
            stmt.setInt(3, product_id);

            if (stmt.executeUpdate() > 0) {
                stmt.close();
                return true;
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
