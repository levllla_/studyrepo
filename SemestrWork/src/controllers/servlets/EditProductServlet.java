package controllers.servlets;

import controllers.configs.ConfigSingleton;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import models.Product;
import models.User;
import repository.ProductRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class EditProductServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String god = request.getParameter("god");
        String model = request.getParameter("model");
        String description = request.getParameter("description");
        int count = Integer.parseInt(request.getParameter("count"));
        int price = Integer.parseInt(request.getParameter("price"));
        int id = Integer.parseInt(request.getPathInfo().substring(1));
        Product product = new Product(id, god, model, description, count, price);
        ProductRepository.editProduct(product);
        response.sendRedirect("/products");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        freemarker.template.Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template template = cfg.getTemplate("/products/edit.ftl");
        HashMap<String, Object> root = new HashMap<>();
        ServletContext context = request.getServletContext();
        User user = (User) context.getAttribute("user");
        Integer productId = Integer.parseInt(request.getPathInfo().substring(1));
        root.put("user", user);
        root.put("product", ProductRepository.getProduct(productId));

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
