package controllers.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import controllers.configs.ConfigSingleton;
import models.User;
import repository.UsersRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;

public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html; charset=UTF-8");
        ServletContext context = getServletContext();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = new User(login, password);
        String authResult = UsersRepository.authUser(user);

        switch (authResult) {
            case "success":
                Cookie loginCookie = new Cookie("login", login);
                loginCookie.setMaxAge(60 * 60 * 24);
                response.addCookie(loginCookie);
                context.setAttribute("user", UsersRepository.getUser(login));
                context.setAttribute("auth", true);
                context.removeAttribute("products");
                response.sendRedirect("/products");
                break;
            default:
                response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        freemarker.template.Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template template = cfg.getTemplate("/welcome/login.ftl");
        HashMap<String, Object> root = new HashMap<>();

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
