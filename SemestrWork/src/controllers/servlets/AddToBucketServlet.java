package controllers.servlets;

import models.Product;
import repository.ProductRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AddToBucketServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getPathInfo().substring(1));
        ServletContext context = getServletContext();

        ArrayList<Product> products = (ArrayList<Product>) context.getAttribute("products");

        if (products == null)
            products = new ArrayList<>();

        products.add(ProductRepository.getProduct(id));
        context.setAttribute("products", products);
        response.sendRedirect("/bucket");
    }
}
