package controllers.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import controllers.configs.ConfigSingleton;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class WelcomeServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingleton.getCfg(getServletContext());

        if (getServletContext().getAttribute("user") != null)
            if (((User) getServletContext().getAttribute("user")).getId() != -1)
                response.sendRedirect("/bucket");

        try {
            cfg.getTemplate("welcome/index.ftl").process(null, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }
}
