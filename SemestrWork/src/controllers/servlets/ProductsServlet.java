package controllers.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import controllers.configs.ConfigSingleton;
import models.Product;
import repository.ProductRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class ProductsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String header = request.getParameter("header");
        String content = request.getParameter("content");
        System.out.println(header + content);
        Product product = new Product(header, content);
        if (ProductRepository.createProduct(product))
            response.sendRedirect("/products/" + ProductRepository.getLastId());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        freemarker.template.Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        HashMap<String, Object> root = new HashMap<>();
        Template template;

        if (request.getPathInfo() == null) {
            template = cfg.getTemplate("/products/index.ftl");
            root.put("products", ProductRepository.getProducts());
        } else if (request.getPathInfo().equals("/new")) {
            template = cfg.getTemplate("/products/new.ftl");
        } else {
            template = cfg.getTemplate("/products/show.ftl");
            int id = Integer.valueOf(request.getPathInfo().substring(1));
            Product product = ProductRepository.getProduct(id);
            root.put("header", product.getHeader());
            root.put("content", product.getContent());
            root.put("id", product.getId());
        }

        root.put("user", getServletContext().getAttribute("user"));

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
