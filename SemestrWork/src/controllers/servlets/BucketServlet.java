package controllers.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import controllers.configs.ConfigSingleton;
import models.Product;
import models.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class BucketServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        freemarker.template.Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template template = cfg.getTemplate("/bucket/index.ftl");
        HashMap<String, Object> root = new HashMap<>();
        ServletContext context = request.getServletContext();
        ArrayList<Product> products = (ArrayList<Product>) context.getAttribute("products");
        User user = (User) context.getAttribute("user");
        Integer summ = 0;

        if (products != null)
            for (Product product : products)
                summ += product.getPrice();

        root.put("user", user);
        root.put("products", products);
        root.put("summ", summ);

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}

