package controllers.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import controllers.configs.ConfigSingleton;
import models.User;
import repository.UsersRepository;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;

public class RegistrationServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String login = request.getParameter("login");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = new User(-1, login, password, firstName, lastName, email);

        System.out.println(user);

        if (UsersRepository.createUser(user)) {
            Cookie loginCookie = new Cookie("login", login);
            loginCookie.setMaxAge(60 * 60 * 24);
            response.addCookie(loginCookie);
            getServletContext().setAttribute("user", UsersRepository.getUser(login));
            getServletContext().setAttribute("auth", true);
            getServletContext().removeAttribute("products");
            response.sendRedirect("/products");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        freemarker.template.Configuration cfg = ConfigSingleton.getCfg(getServletContext());
        Template template = cfg.getTemplate("/welcome/registration.ftl");
        HashMap<String, Object> root = new HashMap<>();

        try {
            template.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
