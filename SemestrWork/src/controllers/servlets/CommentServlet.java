package controllers.servlets;

import models.User;
import repository.CommentRepository;
import repository.ProductRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String content = request.getParameter("content");
        ServletContext context = request.getServletContext();
        String userName = ((User) context.getAttribute("user")).getFirstName();
        int id = Integer.valueOf(request.getPathInfo().substring(1));

        if (CommentRepository.createComment(userName, content, id)){
            response.sendRedirect("/products/" + request.getPathInfo().substring(1));
        }
    }
}
