package controllers.filters;

import models.User;
import repository.UsersRepository;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        ServletContext context = request.getServletContext();
        Cookie[] cookies = request.getCookies();
        User user = (User) context.getAttribute("user");
        response.setContentType("text/html; charset = UTF-8");

        if (user == null && cookies != null)
            for (Cookie cookie : cookies)
                if (cookie.getName().equals("login")) {
                    user = UsersRepository.getUser(cookie.getValue());
                    context.setAttribute("user", user);
                    context.setAttribute("auth", true);
                }

        if (context.getAttribute("auth") == null)
            context.setAttribute("user", User.getGuest());

        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }
}
