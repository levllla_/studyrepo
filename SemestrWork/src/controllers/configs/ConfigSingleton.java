package controllers.configs;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;

public class ConfigSingleton {
    private static Configuration cfg;

    public static Configuration getCfg(ServletContext servletContext){
        if (cfg == null) {
            cfg = new Configuration(Configuration.VERSION_2_3_22);
            cfg.setServletContextForTemplateLoading(servletContext, "/WEB-INF/views");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
            servletContext.setAttribute("cfg", cfg);
        }
        return cfg;
    }
}
