<#ftl encoding="utf-8">
<!doctype html>
<html lang="en">
<head>
<#include "../layouts/styles.ftl">
    <title>Корзина</title>
</head>
<body class="background">
<#include "../layouts/navbar.ftl">
<div class="container margin-100">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="/images/default_avatar.png" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                    ${user.getFullName()}
                    </div>
                    <div class="profile-usertitle-job">
                    ${user.getLogin()}
                    </div>
                </div>
                <hr/>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="#">
                                <i class="glyphicon glyphicon-th-large"></i>
                                Корзина на сумму: ${summ}р.</a>
                        </li>
                    </ul>
                    <br/>
                    <form action="/clearbucket" method="post">
                        <button class="btn btn-sm center-block btn-danger " >Очистить корзину</button>
                    </form>
                </div>
                <hr/>
            </div>
        </div>
        <div class="col-md-9 white">
            <div class="profile-model">
                <h3>Устройства в корзине
                    <#if products??>
                        <button id="buy" class="btn btn-success btn-sm pull-right">Купить</button>
                    </#if>
                </h3>
                <hr/>
            <#if !products??>
                <br/>
                <h4 class="text-center">Список товаров пуст.</h4>
                <br/>
                <hr/>
            <#else>
                <#list products as product>
                    <div class="panel-body">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="http://placehold.it/70x100">
                                </a>
                                <div class="media-body">
                                    <a href="/products/${product.getId()}"><h3 class="media-heading">${product.getModel()}</h3></a>
                                    <h4>${product.getGod()}</h4>
                                    <h5>Цена:  ${product.getPrice()}  руб.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </#list>
            </#if>
            </div>
        </div>
    </div>
</body>

<script>
    $(document).ready(function() {
        $("#buy").click(function(){
            alert("Вы купили товар на сумму ${summ}!");
        });
    });
</script>
</html>