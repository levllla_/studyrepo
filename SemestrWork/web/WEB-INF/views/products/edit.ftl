<#ftl encoding="utf-8">
<!doctype html>
<html lang="ru">
<head>
<#include "../layouts/styles.ftl">
    <title>Изменить данные</title>
</head>
<body class="background">
<#include "../layouts/navbar.ftl">
<div class="col-lg-8 col-lg-offset-2 margin-100">
    <h2>Создание нового события</h2>
    <form action="/edit/${product.getId()}" method="post">
        <label class="control-label">Производитель:</label>
        <input type="text" name="god" class="form-control" value="${product.getGod()}"/><br/>
        <label class="control-label">Модель:</label>
        <input type="text" name="model" class="form-control" value="${product.getModel()}"/><br/>
        <label class="control-label">Характеристики:</label>
        <textarea rows="10" type="text" name="description" class="form-control">${product.getDescription()}</textarea>
        <br/>
        <label class="control-label">Цена: </label>
        <input type="number" name="price" class="form-control" value="${product.getPrice()?string.computer}"/><br/>
        <label class="control-label">Кол-во в наличии: </label>
        <input type="number" name="count" class="form-control" value="${product.getCount()?string.computer}"/><br/>
        <button type="submit" class="btn btn-primary">Изменить</button>
    </form>
    <div class="margin-100"></div>
</div>
</body>
</html>