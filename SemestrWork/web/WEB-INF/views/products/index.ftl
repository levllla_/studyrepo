<#ftl encoding="utf-8">
<!doctype html>
<html>
<head>
<#include "../layouts/styles.ftl">
    <title>Телефоны</title>
</head>
<body class="background">
<#include "../layouts/navbar.ftl">
<div class="container margin-100 clearfix">
    <h2>Мобильные устройства
    <#if user.isAdmin() >
        <a href="/products/new" role="button" class="btn btn-success btn-sm pull-right">Добавить</a>
    </#if></h2>
    <br/>
    <div class="panel panel-default">
    <#list products as product>
        <div class="panel-body">
            <div class="panel-heading">
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/120x200">
                    </a>
                    <div class="media-body">
                        <a href="/products/${product.getId()}"><h4 class="media-heading">${product.getModel()}</h4></a>
                        <h4>${product.getGod()}</h4>
                        <p>${product.getPrice()} руб.</p>
                        <hr/>
                        <p>${product.getShortDesc()}</p>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
    </#list>
    </div>
</div>
</body>
</html>