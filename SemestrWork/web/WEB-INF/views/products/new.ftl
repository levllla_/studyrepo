<#ftl encoding="utf-8">
<!doctype html>
<html lang="ru">
<head>
    <#include "../layouts/styles.ftl">
    <link rel="stylesheet" href="/css/fileinput.css"/>
    <script type="application/javascript" src="/js/fileinput.js"></script>
    <title>Добавить устойство</title>
</head>
<body class="background">
    <#include "../layouts/navbar.ftl">
    <div class="col-lg-8 col-lg-offset-2 margin-100">
        <h2>Создание нового события</h2>
        <form action="" method="post">
            <label class="control-label">Производитель:</label>
            <input type="text" name="god" class="form-control"/><br/>
            <label class="control-label">Модель:</label>
            <input type="text" name="model" class="form-control"/><br/>
            <label class="control-label">Характеристики:</label>
            <textarea rows="10" type="text" name="description" class="form-control"></textarea><br/>
            <label class="control-label">Цена: </label>
            <input type="number" name="price" class="form-control"/><br/>
            <label class="control-label">Кол-во в наличии: </label>
            <input type="number" name="count" class="form-control"/><br/>
            <label class="control-label">Изображение</label>
            <input id="file" type="file" multiple=true class="file-loading col-lg-4" name="file_send"><br/>
            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
        <div class="margin-100"></div>
    </div>

    <script>
        $(document).on('ready', function() {
            $("#file").fileinput({
                showCaption: false,
                showUpload: false
            });
        });
    </script>
</body>
</html>