<#ftl encoding="utf-8">
<!doctype html>
<html>
<head>
    <title>New Page</title>
    <#include "../layouts/styles.ftl">
</head>
<body class="background">
<#include "../layouts/navbar.ftl">
<div class="container margin-100">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel-heading">
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object margin-30" src="http://placehold.it/250x400">
                    </a>
                    <div class="media-body">
                        <h3 class="media-heading"><form action="/add/${product.getId()}" method="post">
                            <button name="foo" class="btn btn-success pull-right">В корзину</button>
                        </form> ${product.getModel()}
                        </h3>
                        <h4>${product.getGod()}</h4>
                        <hr>
                        <p>${product.getDescription()}</p>
                    </div>
                    <#if user.isAdmin() >
                    <form action="/delete/${product.getId()}" method="post">
                        <button class="btn btn-sm btn-danger pull-right clearfix" >Удалить</button>
                    </form>
                    <form action="/edit/${product.getId()}" method="get">
                        <button name="foo" class="btn btn-sm btn-primary pull-right clearfix">Изменить</button>
                    </form>
                    </#if>
                </div>
                <#if user.getId() != -1>
                <div class="well margin-30">
                    <h4>Оставить отзыв:</h4>
                    <form role="form" method="post" action="/add_comment/${product.getId()}">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="content"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </form>
                </div>
                </#if>
                <h3>Комментарии:</h3>
                <hr/>
                <#list comments as comment>
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="http://placehold.it/64x64" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">${comment.getAuthor()}</h4>
                        ${comment.getContent()}
                        </div>
                    </div>
                    <hr/>
                </#list>
                </div>
            </div>
        </div>
        <hr/>
    </div>
</body>
</html>