<#ftl encoding="utf-8">
<!DOCTYPE html>
<html lang="en">
<head>
<#include "../layouts/styles.ftl">
    <meta charset="UTF-8">
    <title>Регистрация</title>
</head>
<body class="background">
<div class="container container col-lg-4 col-lg-offset-4  margin-100">
    <h1 class="text-center">Регистрация</h1>

    <form role="form" action="/registration" method="post">
        <input type="text" name="first_name" class="form-control required" placeholder="Имя" required><br>
        <input type="text" name="last_name" class="form-control" placeholder="Фамилия" required><br>
        <input type="text" name="login" class="form-control" placeholder="Логин" required><br>
        <input type="email" name="email" class="form-control" placeholder="Email адрес" required><br>
        <input type="password" name="password" class="form-control" placeholder="Пароль" required><br>
        <button type="submit" class="btn btn-primary pull-right">Регистрация</button>
    </form>
</div>
</body>
</html>