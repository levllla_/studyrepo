<#ftl encoding="utf-8">
<!DOCTYPE html>
<html lang="en">
<head>
<#include "../layouts/styles.ftl">
  <meta charset="UTF-8">
  <title>Вход</title>
</head>
<body class="background">
<div class="container col-lg-4 col-lg-offset-4  margin-100">
    <h1 class="text-center">Вход</h1>
    <form role="form" method="post" action="/login">
        <div class="form-group">
            <label for="login">Логин:</label>
            <input type="text" class="form-control" name="login">
        </div>
        <div class="form-group">
            <label for="pwd">Пароль:</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Запомнить меня</label>
        </div>
        <button type="submit" class="btn btn-primary pull-right">Вход</button>
    </form>
</div>
</body>
</html>