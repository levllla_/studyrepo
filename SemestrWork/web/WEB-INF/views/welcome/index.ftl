<#ftl encoding="utf-8">
<!doctype html>
<html lang="ru">
<head>
<#include "../layouts/styles.ftl">
    <script type="application/javascript" src="/js/jquery.validate.js"></script>
    <title>Mobile.Store</title>
</head>
<body class="background">
<nav class="navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/bucket">Корзина</a></li>
            <li><a href="/products">Каталог</a></li>
            <li><a href="/login">Войти</a></li>
        </ul>
    </div>
</nav>

<div class="row">
    <div class="col-lg-4 col-lg-offset-6 margin-10p">
        <h1>Mobile.Store</h1>
        <p>
            Магазин мобильних устройств, который предоставляет пользователям обширный каталог девайсов разных характеристик.
            Сервис дает возможность пользователям просматривать каталог и получать подробную информацию о каждом устройсве.
            Так же пользователи могут сделать заказ уйстойства, которое ему понравится и наслаждаться им вживую, а после - оставить
            отзыв.
        </p>
        <a href="/registration" class="btn btn-info" role="button">Регистрация</a>
    </div>
</div>


</body>
</html>