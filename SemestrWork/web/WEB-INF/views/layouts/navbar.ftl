<#ftl encoding="utf-8">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-god">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand newfont" href="/">Mobile.Store</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/bucket">Корзина</a></li>
                <li><a href="/products">Каталог</a></li>
                <#if user.getId() == -1>
                <li><a href="/login">Войти</a></li>
                <#else>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="/images/default_avatar.png" class="user-image img-circle"> &nbsp;&nbsp;&nbsp; ${user.getFullName()} &nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/logout">Выход</a></li>
                    </ul>
                </li>
                </#if>
            </ul>
        </div>
    </div>
</nav>