create table "users" (
	"id" serial not null primary key,
	"username" varchar(20) not null,
	"first_name" varchar(50) not null,
	"last_name" varchar(50) not null,
	"password" varchar(15) not null,
	"email" varchar(50) not null,
	"admin" int default 0
);

create table "products" (
	"id" serial not null primary key,
	"manufacturer" varchar(40) not null,
	"model" varchar(40) not null,
	"description" varchar(1000) not null,
	"price" int not null,
	"available" int default 0
);

create table "comments" (
	"id" serial not null primary key,
	"user_name" varchar(40) not null,
	"content" varchar(200) not null,
	"product_id" int
);
